import java.util.Arrays;

public class QuickSort {
	
	public static void main(String[] args) {
		int [] array = {5,6,1,3,2,4};
		
		sort(array, 0, array.length - 1);
		System.out.println(Arrays.toString(array));
	}

	private static void sort(int[] array, int esq, int dir) {
		
		if (esq < dir) {
			int j = separar(array, esq, dir);
			sort(array, esq, j-1);
			sort(array, j+1, dir);
		}
		
	}

	private static int separar(int[] array, int esq, int dir) {
		int i = esq;
		int j = dir;
		
		while (i < j) {
			
			while (i < dir && array[i] <= array[esq]) i++;
			while (j > esq && array[j] >= array[esq]) j--;
			
			if (i < j){
				trocar(array, i, j);
				i++;
				j--;
			}
		}
		
		trocar(array, esq, j);
		
		return j;
	}

	private static void trocar(int[] array, int i, int j) {
		int aux = array[i];
		
		array[i] = array[j];
		array[j] = aux;
	}

}
