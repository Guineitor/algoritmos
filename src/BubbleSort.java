import java.util.Arrays;

public class BubbleSort {
	
	public static void main(String[] args) {
	
		int [] array = {5,3,2,4,7,1,0,6};
		
		for (int i = 0; i < array.length - 1; i++) {
			itera(array);
		}
		
		System.out.println("Final: " + Arrays.toString(array));
		
	}

	private static void itera(int[] array) {
		
		for (int i = 0; i < array.length - 1; i ++) {
			compara(array, i, (i + 1));
			System.out.println("Iterou: " + array);
		}
		
	}

	private static void compara(int[] array, int prev, int next) {
		
		if (array[prev] > array[next]) {
			troca(array, prev, next);
		}
		
	}

	private static void troca(int[] array, int prev, int next) {
		int aux = array[prev];
		
		array[prev] = array[next];
		array[next] = aux;
	}

}
